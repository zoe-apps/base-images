#!/bin/sh

set -e

VERSION=${VERSION:-test}
REPOSITORY=${REPOSITORY:-zapps}

echo "building base image"
docker build --pull -t $REPOSITORY/base:$VERSION base

echo "building java image"
docker build --build-arg VERSION=$VERSION -t $REPOSITORY/java:$VERSION java

echo "building python image"
docker build --build-arg VERSION=$VERSION -t $REPOSITORY/python:$VERSION python

